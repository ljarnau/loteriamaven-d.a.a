package es.eug.daw.ed.loteria.maven.LoteriaMaven;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Test unitarios para el programa.
 */
public class AppTest extends TestCase {
	
	//Test con fail prefijado
	public void testFail() {
		fail("Estamos trabajando en ello");
	}
    
	//Test de los constructores de boletos introducidos manualmente.
    public void testAppPredefinido() {
    	AdministracioLoteria a=new AdministracioLoteria("1 1 1 1 1 1 C");
    	String[]test= {"1 1 1 1 1 1 A","1 1 1 1 1 1 B","1 1 1 1 1 1 C"};
    	ParticipantLoteria b=new ParticipantLoteria(test);
        assertTrue(a.winner(b));
    	String[]test2= {"1 1 1 1 1 1 A","1 1 1 1 1 1 B","1 1 1 1 1 1 D"};
    	ParticipantLoteria c=new ParticipantLoteria(test2);
    	assertFalse(a.winner(c));
    }
    
    //Test de los constructores de boletos aleatorios.
    public void testAppRandom() {
    	AdministracioLoteria a=new AdministracioLoteria();
    	ParticipantLoteria b=new ParticipantLoteria();
        assertFalse(a.winner(b));
        /*assertTrue no se puede probar debido a que al ser aleatorio es imposible
         determinar que numero sera igual en ambos casos.*/
    }
    
    /*Test que cubre el 100% de coverage para contemplar diferentes entradas en el 
     constructor que genera boletos aleatorios.*/
    public void testAppConstructor() {
    	AdministracioLoteria a=new AdministracioLoteria();
    	ParticipantLoteria b=new ParticipantLoteria();
    	AdministracioLoteria c=new AdministracioLoteria();
    	ParticipantLoteria d=new ParticipantLoteria();
    	AdministracioLoteria e=new AdministracioLoteria();
    	ParticipantLoteria f=new ParticipantLoteria();
    	AdministracioLoteria g=new AdministracioLoteria();
    	ParticipantLoteria h=new ParticipantLoteria();
    }
    
    /*Test que comprueba el caso de que sea un boleto null*/
    public void testAppNull() {
    	AdministracioLoteria a=new AdministracioLoteria();
    	assertFalse(a.winner(null));
    }
}
