package es.eug.daw.ed.loteria.maven.LoteriaMaven;
import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * Clase encargada de generar el boleto ganador de la Loteria Maven S.A.
 * @author David Benavente, Arnau Llamas, Alejandro Maldonado
 * @version 1.0
 */
public class ParticipantLoteria {
	//Creamos objeto de la clase log para poder generar el archivo app.log
	private static final Logger log = LogManager.getLogger("ParticipantLoteria");
	int numboletos=(int) (Math.random()*(10-1+1)*(100)%(10));
    private String[]combinacions=new String[numboletos];
    /**
	 * Metodo constructor que genera como minimo 1 y como maximo 10 boletos
	 * de loteria de manera aleatoria. Los numeros de los boletos tambien son
	 * generados aleatoriamente.
	 */
    public ParticipantLoteria() {
    	log.trace("Constructor con numero random");
        String[]alph= {"A","B","C","D","E","F","G","H"};
        String[]boletos=new String[numboletos];
        for(int l=0;l<boletos.length;l++) {
        	log.info("Boleto generado");
            String com="";
            int i=0,elementos=6;
            int numalph=(int) (Math.random()*(7-0+1)*(100)%(7));
            int[]num=new int[elementos];
            num[i]=(int) (Math.random()*(50-0+1)*(100)%(50));
            log.debug("Elemento boleto random");
            for(i=1;i<elementos;i++) {
            	log.info("Elemento boleto random");
                num[i]=(int) (Math.random()*(50-0+1)*(100)%(50));
                for(int j=0;j<i;j++) {
                    if(num[i]==num[j]) {
                    	log.warn("Repeticion numero elemento");
                        i--;
                    }
                }
            }
            for(int k=0;k<num.length;k++) {
                com=com+num[k]+" ";
            }
            this.combinacions[l]=com+alph[numalph];
        }
    }
    /**
     * Metodo get que hace que se pueda obtener desde una classe externa
     * los boletos del participante. 
	 * @return Retorna combinaciones que es el array de boletos 
	 * del participante.
     */
    public String[] getCombinacions() {
    	log.debug("Acceso boleto participante");
        return combinacions;
    }
    /**
	 * Metodo constructor test que sirve para asignar de manera manual
	 * el boleto del participante para poder asignarle el boleto ganador y
	 * comprobar el correcto funcionamiento del programa
	 * @param test El parametro "test" es el boleto del participante que 
	 * introducimos manualmente.
	 */
    public ParticipantLoteria(String[] test) {
    	log.trace("Constructor test");
        this.combinacions=test;
    }
    /**
	  * Metodo toString para poder mostrar en pantalla los boletos
	  * del participante y el boleto ganador. Tambien se muestra si 
	  * se ha ganado o no.
	  */
   @Override
   	//Para conseguir un coverage del 100% es necesario comentar
	//todo el metodo para que este no lo cuente.
    public String toString() {
        return String.format("ParticipantLoteria [combinacions=%s] ", Arrays.toString(combinacions));
    }
}