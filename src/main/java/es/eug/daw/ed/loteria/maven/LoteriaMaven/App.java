package es.eug.daw.ed.loteria.maven.LoteriaMaven;
/**
 * Clase Main para mostrar el resultado del programa.
 * Declaramos objetos de las clases y probamos los diferentes
 * metodos del programa.
 * @author David Benavente, Arnau Llamas, Alejandro Maldonado
 *@version 1.0
 */
public class App {
	
    public static void main( String[] args ) {
    	//Funcionamiento de los constructores aleatorios.
    	AdministracioLoteria a=new AdministracioLoteria();
        ParticipantLoteria b=new ParticipantLoteria();
        System.out.println(a);
        System.out.println(b);
        System.out.println(a.winner(b));
        //Funcionamiento de los constructores test
        AdministracioLoteria c=new AdministracioLoteria("1 1 1 1 1 1 C");
        String[]test= {"1 1 1 1 1 1 A","1 1 1 1 1 1 B","1 1 1 1 1 1 C"};
        ParticipantLoteria d=new ParticipantLoteria(test);
        System.out.println(c);
        System.out.println(d);
        System.out.println(c.winner(d));
    }
}