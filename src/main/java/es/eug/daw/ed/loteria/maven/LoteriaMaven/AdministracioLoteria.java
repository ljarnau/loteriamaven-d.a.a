package es.eug.daw.ed.loteria.maven.LoteriaMaven;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * Clase encargada de generar el boleto ganador de la Loteria Maven S.A.
 * @author David Benavente, Arnau Llamas, Alejandro Maldonado
 * @version 1.0
 */
public class AdministracioLoteria { 
	//Creamos objeto de la clase log para poder generar el archivo app.log  
	private static final Logger log = LogManager.getLogger("AdministracioLoteria"); 
	private String numwinner;
	/**
	 * Metodo constructor que genera un numero ganador aleatorio.
	 */
	public AdministracioLoteria() {
		log.trace("Constructor con numero random");
		int i=0,elementos=6;
	    String com="";
	    String[]alph= {"A","B","C","D","E","F","G","H"};
	    int numalph=(int) (Math.random()*(8-0+1)*(100)%(8));
	    int[]num=new int[elementos];
        num[i]=(int) (Math.random()*(50-0+1)*(100)%(50));
        log.debug("Elemento boleto random");
	    for(i=1;i<num.length;i++) {
	    	log.info("Elemento boleto random");
	    	num[i]=(int) (Math.random()*(50-0+1)*(100)%(50));
	        for(int j=0;j<i;j++) {
	        	if(num[i]==num[j]) {
	        		log.warn("Repeticion numero elemento");
	        		i--;
	        	}
	        }
	     }
	     for(int k=0;k<num.length;k++) {
	    	 com=com+num[k]+" ";
	     }
	     com=com+alph[numalph];
	     this.numwinner=com;
	   	 }
	/**
	 * Metodo que determina si el participante ha sido afortunado y 
	 * tiene el boleto ganador, o por lo contrario ha perdido.
	 * @param a El parametro "a" son los boletos del participante.
	 * @return Retorna un valor boleano que indica true si el 
	 * boleto generado es el mismo que el del participante o false
	 * si no lo es.
	 */
	public boolean winner(ParticipantLoteria a) {
        //Condicion que tiene en cuenta si el valor es nulo.
		if(a==null) {
        	log.error("Objeto es igual a null");
        	return false;
        }
		boolean w=false;
        for(int i=0;i<a.getCombinacions().length;i++) {
            if(this.numwinner==a.getCombinacions()[i]) {
            	log.info("Boleto encontrado");
                w=true;
            }else {
            	log.info("Boleto no encontrado");
            }
            log.debug("Boleto comparado");
        }
        return w;
	}
	/**
	 * Metodo constructor test que sirve para comprobar el correcto
	 * funcionamiento del programa, forzando al mismo a tener un 
	 * boleto ganador.
	 * @param test El parametro "test" es el boleto ganador que 
	 * introducimos manualmente.
	 */
	 public AdministracioLoteria(String test) {
		 log.trace("Constructor test");
	        this.numwinner=test;
	 }
	 /**
	  * Metodo toString para poder mostrar en pantalla los boletos
	  * del participante y el boleto ganador. Tambien se muestra si 
	  * se ha ganado o no.
	  */
	 @Override
	 //Para conseguir un coverage del 100% es necesario comentar
	 //todo el metodo para que este no lo cuente.
	 public String toString() {
		 return String.format("AdministracioLoteria [numwinner=%s] ", numwinner);
	 }
}